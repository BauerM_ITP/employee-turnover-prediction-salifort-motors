"""Utility functions for the capstone project"""

import seaborn as sns
import pandas as pd
import numpy as np
from sklearn.metrics import (
    accuracy_score,
    precision_score,
    recall_score,
    f1_score,
    confusion_matrix,
    ConfusionMatrixDisplay,
)
from sklearn.model_selection import GridSearchCV
import matplotlib.pyplot as plt


def desaturate_palette(palette, desat: float):
    """Desaturate a seaborn palette"""
    return [sns.desaturate(color, desat) for color in palette]


def get_model_scores(model: str, y_pred, y_test):
    """Returns Accuracy, Precision, Recall, and F1 scores for a model"""
    accuracy = accuracy_score(y_test, y_pred)
    precision = precision_score(y_test, y_pred)
    recall = recall_score(y_test, y_pred)
    f1 = f1_score(y_test, y_pred)
    return pd.DataFrame(
        {
            "model": [model],
            "accuracy": [accuracy],
            "precision": [precision],
            "recall": [recall],
            "f1": [f1],
        }
    )


def feature_engineering(df0: pd.DataFrame):
    """Feature engineering for the HR dataset.

    Change column names to snake_case, drop duplicates and create new features and
    encode categorical variables.

    args:s
        df0 : pd.DataFrame : the HR dataset
    returns:
        df : pd.DataFrame : the HR dataset with feature engineering applied
    """

    df_cpy = df0.copy()
    df_cpy.rename(
        columns={
            "average_montly_hours": "average_monthly_hours",
            "number_project": "number_projects",
            "Work_accident": "work_accident",
            "promotion_last_5years": "promotion_last_5_years",
            "time_spend_company": "time_spent_company",
            "Department": "department",
        },
        inplace=True,
    )

    df_cpy.drop_duplicates(inplace=True)

    df_dummy = pd.get_dummies(df_cpy, columns=["department"], drop_first=False)

    df_dummy["salary"] = df_dummy["salary"].map({"low": 0, "medium": 1, "high": 2})

    df_dummy["time_per_project"] = (
        df_dummy["average_monthly_hours"] / df_dummy["number_projects"]
    )

    return df_dummy


def plot_cms(
    y_pred: pd.Series, y_pred_dt_nosat: pd.Series, y_test: pd.Series, model_name: str
):
    """
    Plots confusion matrices for a model and a model without satisfaction
    """
    fig, ax = plt.subplots(1, 2, figsize=(12, 6))

    cm = confusion_matrix(y_test, y_pred)
    disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=["Stayed", "Left"])
    disp.plot(ax=ax[0], cmap="Blues", colorbar=False)
    ax[0].set_title(f"{model_name}")

    cm = confusion_matrix(y_test, y_pred_dt_nosat)
    disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=["Stayed", "Left"])
    disp.plot(ax=ax[1], cmap="Blues", colorbar=False)
    ax[1].set_title(f"{model_name} (without satisfaction)")


def plot_feature_importance(
    features: pd.Series,
    grid_search: GridSearchCV,
    features_nosat: pd.Series,
    grid_search_nosat: GridSearchCV,
    model: str,
):
    """
    Plots feature importance for a model and a model without satisfaction
    """
    fig, ax = plt.subplots(2, 1, figsize=(12, 12))

    importances = grid_search.best_estimator_.feature_importances_
    indices = np.argsort(importances)[::-1]

    sns.barplot(
        y=np.array([features[i] for i in indices]),
        x=importances[indices],
        ax=ax[0],
        palette="viridis",
    )
    ax[0].set_title(f"{model}")

    importances = grid_search_nosat.best_estimator_.feature_importances_
    indices = np.argsort(importances)[::-1]

    sns.barplot(
        y=np.array([features_nosat[i] for i in indices]),
        x=importances[indices],
        ax=ax[1],
        palette="viridis",
    )
    ax[1].set_title(f"{model} (without satisfaction)")

    plt.show()
