# Employee Turnover Prediction - Salifort Motors

Capstone project for the Google Advanced Data Analytics Professional Certificate.

Dependencies are listed in the `'environment.yml'` file.

## Overview
The goal of this project is to analyze and predict employee turnover at Salifort Motors (fictional). The dataset contains information about employees such as satisfaction level, number of projects, average monthly hours, time spent at the company, work accidents, promotion in the last 5 years, salary, and whether the employee has left the company or not (original data form [Kaggle](https://www.kaggle.com/datasets/mfaisalqureshi/hr-analytics-and-job-prediction)). We find that turnover is high amongst employees with low satisfaction, high monthly ours and no promotions. Tree based classification models were build to predict a turnover with $F1 = 0.95$ and an accuracy of $0.98$.

## Data

Exploration and analysis of the data shows three distinct groups of employees who left the company:

1. **Highly dissatisfied employees with high evaluation scores**: These employees have low satisfaction levels, high monthly hours, and no promotions.

3. **Dissatisfied employees with low evaluation scores**: These employees have high satisfaction levels, low monthly hours, and no promotions.

3. **Satisfied employees with high evaluation scores**: These employees have high satisfaction levels, but high monthly hours and still leave the company.

The categories 1 and 3 are clearly very problematic, as they are leaving the company despite high evaluation scores. The employees in these group also show above average working hours, and lower promotion rates compared to the total population. See the `'eda.ipynb'` for more details.

## Model

To classify and predict whether an employee will leave the company, we build a simple decision three, a random forest and a LightGBM based boosting model. All models perform will with F1 sores of roughly $0.95$ and an accuracy of $0.98$. The random forest slightly outperforms the other models. We also train the models without the satisfaction level as a feature, which results in only a slight decrease in the F1 score to $0.92$. The feature importance analysis confirms the findings of the exploratory data analysis, with satisfaction and workload being the most important features. The promotion rate does not play a significant role, as it is low across the company and thus not a good predictor for outcomes of individual employees. Model training and evaluation is performed in the `'model.ipynb'` notebook.